#Program to compress and decompress files
from zipfile import ZipFile
import os

#Creating a zip file object and also mentioning the file that needs to be extracted


def Extract():
  with ZipFile('Syllabus.zip') as zipextract:
  #Extract the contents of the file in the current directory
     zipextract.extractall()


def Extract_in_folder():
  with ZipFile('Syllabus.zip') as zipextract1:
    #Extracting the file in different directory 
    zipextract1.extractall('Extract')


def Extract_singlefile():
  with ZipFile('Syllabus(2).zip') as zipextract2:
    #Getting list of file names archived
    filenames=zipextract2.namelist()
    #Iterate over each of the files
    for name in filenames:
      #checking if filename ends with .txt
      if name.endswith('.txt') :
        zipextract2.extract(name,'one_file_extracted')


def getfilepath(directory):
  file_paths =[]
  #going through all the subdirectories in the directory
  for root,directories,files in os.walk(directory):
    for nameoffiles in files:
      #joining two string for full file path
      filepath= os.path.join(root,nameoffiles)
      file_paths.append(filepath)

  return file_paths    




#folder that needs to be zipped
directory = 'folder_getting_zipped'
#calling function to get all file paths in directory
Filepath = getfilepath(directory)

#printing names of file getting zipped
print("List of files getting zipped:")
for nameoffile in Filepath:
  print(nameoffile)

#writing files to zip file
with ZipFile('folder_getting_zipped.zip','w') as zip:
  for file in Filepath:
    zip.write(file)  

#function to extract file in the same directory
Extract()

#function to extract file in different directory
Extract_in_folder()

#function to etract a single file from the zipped folder
Extract_singlefile()